﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public Gesture gesture;
	public GameObject lineRendererObj;
	public GameObject gameOverPanel;
	public GameObject saveTemplatePanel;
	public GameObject saveTemplateInputTextObj;
	public GameObject titleText;
	public GameObject menuStartGameButton;
	public GameObject recordTemplateHintText;
	public GameObject scoreTextObj;
	public GameObject currentFigureTextObj;
	public GameObject timeTextObj;
	public GameObject gameTextObj;
	public GameObject gameOverTextObj;
	public int timeForDrawing = 30;
	public float timeDecrementPercentage = 0.05f;
	public static int currentLevel = 0;
	public static bool isGameActive = false;

	private List<Vector2> newTemplateArr;
	private int timeLeftForDrawing;
	private int figureCounter;
	private GestureTemplates gestureTemplates;
	private LineRenderer lineRenderer;
	private Text scoreText;
	private Text currentFigureText;
	private Text timeText;
	private Text gameText;
	private Text gameOverText;
	private Text saveTemplateInputText;

	void Awake() {
		gestureTemplates = GestureTemplates.GetGestureTemplates();
		lineRenderer = lineRendererObj.GetComponent<LineRenderer>();
		scoreText = scoreTextObj.GetComponent<Text>();
		currentFigureText = currentFigureTextObj.GetComponent<Text>();
		timeText = timeTextObj.GetComponent<Text>();
		gameText = gameTextObj.GetComponent<Text>();
		gameOverText = gameOverTextObj.GetComponent<Text>();
		saveTemplateInputText = saveTemplateInputTextObj.GetComponent<Text>();
		toggleGamePanel(false);
	}

	public void ResetGame() {
		gameOverPanel.SetActive(false);
		gameTextObj.SetActive(true);
		recordTemplateHintText.SetActive(true);
		NewGame();
	}

	private void NewGame() {
		figureCounter = 1;
		currentLevel = 0;
		timeLeftForDrawing = timeForDrawing;
		scoreText.text = RemoveDigits(scoreText.text) + currentLevel;
		currentFigureText.text = RemoveDigits(currentFigureText.text) + figureCounter;
		timeText.text = RemoveDigits(timeText.text) + Convert.ToString(timeLeftForDrawing);
		gameText.text = "";
		toggleGamePanel(true);
		RenderFigure();
		InvokeRepeating("TimeCount", 1f, 1f);
		isGameActive = true;
	}

	public void NextStage(string result) {
		gameText.text = result;
		InvokeRepeating("RemoveTextHistory", 2f, 1f);
		++currentLevel;
		++figureCounter;
		scoreText.text = RemoveDigits(scoreText.text) + currentLevel;
		currentFigureText.text = RemoveDigits(currentFigureText.text) + figureCounter;
		CancelInvoke("TimeCount");
		timeLeftForDrawing = (int)(timeForDrawing - (timeForDrawing * timeDecrementPercentage * currentLevel));
		timeText.text = RemoveDigits(timeText.text) + Convert.ToString(timeLeftForDrawing);
		RenderFigure();
		InvokeRepeating("TimeCount", 1f, 1f);
	}

	private void GameOver() {
		toggleGamePanel(false);
		gameTextObj.SetActive(false);
		recordTemplateHintText.SetActive(false);
		gameOverPanel.SetActive(true);
		gameOverText.text = RemoveDigits(gameOverText.text) + currentLevel;
	}

	private void RenderFigure() {
		List<Vector2> figure = gestureTemplates.Templates[currentLevel];
		
		for (int i = 0; i < figure.Count; i++) {
			lineRenderer.SetPosition(i, new Vector3(figure[i].x, figure[i].y, 0f));
        }
	}

	private void TimeCount() {
		if (timeLeftForDrawing == 0) {
			CancelInvoke("TimeCount");
			GameOver();
		} else {
			timeLeftForDrawing--;
			timeText.text = RemoveDigits(timeText.text) + Convert.ToString(timeLeftForDrawing);
		}
	}

	private void RemoveTextHistory() {
		CancelInvoke("RemoveTextHistory");
		gameText.text = "";
	}

	public void OnStartGamePressed() {
		titleText.SetActive(false);
		menuStartGameButton.SetActive(false);
		NewGame();
    }

	public void ShowSaveTemplatePanel(List<Vector2> newTemplateArr) {
		this.newTemplateArr = newTemplateArr;
		toggleGamePanel(false);
		gameTextObj.SetActive(false);
		saveTemplatePanel.SetActive(true);
	}

	public void OnConfirmSaveTemplateButtonPressed() {
		gestureTemplates.Templates.Insert(0, newTemplateArr);
		gestureTemplates.TemplateNames.Insert(0, saveTemplateInputText.text);
		gameText.text = "Жест: " + saveTemplateInputText.text +  "\n Сохранен.";
		saveTemplateInputText.text = "";
		saveTemplatePanel.SetActive(false);
		gestureTemplates.SaveTemplatesToFile();
		ResetGame();
	}

	public void OnCancelSaveTemplateButtonPressed() {
		saveTemplateInputText.text = "";
		newTemplateArr.Clear();
		saveTemplatePanel.SetActive(false);
		ResetGame();
	}

	public void toggleGamePanel(bool isActive) {
		scoreTextObj.SetActive(isActive);
		currentFigureTextObj.SetActive(isActive);
		timeTextObj.SetActive(isActive);
		lineRendererObj.SetActive(isActive);
	}

	private string RemoveDigits(string key) {
		return Regex.Replace(key, @"\d", "");
	}

	void Update() {
		if (Input.GetKey(KeyCode.Escape)) {
			CancelInvoke("TimeCount");
			Application.Quit();
		}
	}
}
