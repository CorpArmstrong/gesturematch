﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Gesture : MonoBehaviour {

	public GameObject draw;
	public static int mouseDown;

	private Vector2 p;
	private List<Vector2> pointArr;	
	private GestureRecognizer gestureRecognizer;

	void Awake() {
		gestureRecognizer = new GestureRecognizer();
		pointArr = new List<Vector2>();
	}
	
	void Update() {
		if (Input.GetMouseButtonDown(0) && GameController.isGameActive) {
			mouseDown = 1;
		}
		
		if (mouseDown == 1) {
			p = new Vector2(Input.mousePosition.x , Input.mousePosition.y);
			pointArr.Add(p);
			StartCoroutine(worldToScreenCoordinates());
		}
		
		if (Input.GetMouseButtonUp(0)) {
			// if CTRL key is held down, the script will record a gesture. 
			if (Input.GetKey (KeyCode.LeftControl)) {
				mouseDown = 0;
				GameController.isGameActive = false;
				gestureRecognizer.RecordTemplate(pointArr);
			} else {
				if (pointArr.Count > 0) {
					if (GameController.isGameActive) {
						mouseDown = 0;
						gestureRecognizer.StartRecognizer(pointArr);
						pointArr.Clear();
					}
				}
			}
		}
    }

    IEnumerator worldToScreenCoordinates() {
        // fix world coordinate to the viewport coordinate
		Vector3 screenSpace = Camera.main.WorldToScreenPoint(draw.transform.position);
		
		while (Input.GetMouseButton(0)) {
			Vector3 curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);
			Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenSpace); 
			draw.transform.position = curPosition;
            yield return null;
		}
	}
}
