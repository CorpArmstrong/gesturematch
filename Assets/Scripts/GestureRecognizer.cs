﻿/*
 * Based on script (JS) from '(c) viezel studios'
 */

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class GestureRecognizer {
	
	public GameObject gcontrol;	
	private GestureTemplates gestureTemplates;
	private GameController gameController;
	
	private int maxPoints = 64;					// max number of point in the gesture
	private int sizeOfScaleRect = 500;			// the size of the bounding box
	private int compareDetail = 15;				// Number of matching iterations (CPU consuming) 
	private float angleRange = 45f;				// Angle detail level of when matching with templates 
	private string outputText;
	private List<Vector2> newTemplateArr;

	public GestureRecognizer() {
		gestureTemplates = GestureTemplates.GetGestureTemplates();
		gcontrol = GameObject.Find("GameController");
		gameController = gcontrol.GetComponent<GameController>();
	}
	
	public void StartRecognizer(List<Vector2> pointArray) {
		pointArray = OptimizeGesture(pointArray, maxPoints);
		Vector2 center = CalcCenterOfGesture(pointArray);
		float radians = Mathf.Atan2(center.y - pointArray[0].y, center.x - pointArray[0].x);
		pointArray = RotateGesture(pointArray, -radians, center);
		pointArray = ScaleGesture(pointArray, sizeOfScaleRect);
		pointArray = TranslateGestureToOrigin(pointArray);
		GestureMatchForCurrentTemplate(pointArray);
	}

	public void RecordTemplate(List<Vector2> pArray) {
		pArray = OptimizeGesture(pArray, maxPoints);
		Vector2 center = CalcCenterOfGesture(pArray);
		float radians = Mathf.Atan2(center.y - pArray[0].y, center.x - pArray[0].x);
		pArray = RotateGesture(pArray, -radians, center);
		pArray = ScaleGesture(pArray, sizeOfScaleRect);
		pArray = TranslateGestureToOrigin(pArray);
		newTemplateArr = new List<Vector2>(pArray);
		gameController.SendMessage("ShowSaveTemplatePanel", newTemplateArr);
	}

	private List<Vector2> OptimizeGesture(List<Vector2> pointArray, int maxPoints) {
		// take all the points in the gesture and finds the correct points compared with distance and the maximun value of points
		
		// calc the interval relative the length of the gesture drawn by the user
		float interval = CalcTotalGestureLength(pointArray) / (maxPoints - 1);
		
		// use the same starting point in the new array from the old one. 
		List<Vector2> optimizedPoints = new List<Vector2> { pointArray[0] };
		
		float tempDistance = 0.0f;
		
		// run through the gesture array. Start at i = 1 because we compare point two with point one)
		for (int i = 1; i < pointArray.Count; i++)	{
			float currentDistanceBetween2Points = CalcDistance(pointArray[i - 1] , pointArray[i]);
			
			if ((tempDistance + currentDistanceBetween2Points) >= interval) {
				
				// the calc is: old pixel + the differens of old and new pixel multiply  
				float newX = pointArray[i - 1].x + ((interval - tempDistance) / currentDistanceBetween2Points) * (pointArray[i].x - pointArray[i - 1].x);
				float newY = pointArray[i - 1].y + ((interval - tempDistance) / currentDistanceBetween2Points) * (pointArray[i].y - pointArray[i - 1].y);
				
				// create new point
				Vector2 newPoint = new Vector2(newX, newY);
				
				// set new point into array
				optimizedPoints.Add(newPoint);

				pointArray.Insert(i, newPoint);
				
				tempDistance = 0.0f;
			} else {
				// the point was too close to the last point compared with the interval,. Therefore the distance will be stored for the next point to be compared.
				tempDistance += currentDistanceBetween2Points;
			}
		}
		
		// Rounding-errors might happens. Just to check if all the points are in the new array
		if (optimizedPoints.Count == maxPoints - 1) {
			optimizedPoints.Add(new Vector2(pointArray[pointArray.Count - 1].x, pointArray[pointArray.Count - 1].y));
		}

		return optimizedPoints;
	}
	
	private List<Vector2> RotateGesture(List<Vector2> pointArray, float radians, Vector2 center)  {
		// loop through original array, rotate each point and return the new array
		List<Vector2> newArray = new List<Vector2>();
		float cos = Mathf.Cos(radians);
		float sin = Mathf.Sin(radians);
		
		for (int i = 0; i < pointArray.Count; i++) {
			float newX = (pointArray[i].x - center.x) * cos - (pointArray[i].y - center.y) * sin + center.x;
			float newY = (pointArray[i].x - center.x) * sin + (pointArray[i].y - center.y) * cos + center.y;

			newArray.Add(new Vector2(newX, newY));
		}

		return newArray;
	}
	
	private List<Vector2> ScaleGesture(List<Vector2> pointArray, int size) {
		
		// equal min and max to the opposite infinity, such that every gesture size can fit the bounding box.
		var minX = Mathf.Infinity;
		var maxX = Mathf.NegativeInfinity; 
		var minY = Mathf.Infinity;
		var maxY = Mathf.NegativeInfinity;
		
		// loop through array. Find the minimum and maximun values of x and y to be able to create the box
		for (var i = 0; i < pointArray.Count; i++) {
			if (pointArray[i].x < minX) {
				minX = pointArray[i].x;
			}
			if (pointArray[i].x > maxX) {
				maxX = pointArray[i].x;
			}
			if (pointArray[i].y < minY) {
				minY = pointArray[i].y;
			}
			if (pointArray[i].y > maxY) {
				maxY = pointArray[i].y;
			}
		}
		
		// create a rectangle surronding the gesture as a bounding box.
		Rect BoundingBox = new Rect(minX, minY, maxX - minX, maxY - minY);
		
		List<Vector2> newArray = new List<Vector2>();
		
		for (int i = 0; i < pointArray.Count; i++) {
			float newX = pointArray[i].x * (size / BoundingBox.width);
			float newY = pointArray[i].y * (size / BoundingBox.height);
			newArray.Add(new Vector2(newX, newY));
		}

		return newArray;
	}
	
	private List<Vector2> TranslateGestureToOrigin(List<Vector2> pointArray) {
		Vector2 origin = new Vector2(0,0);
		Vector2 center = CalcCenterOfGesture(pointArray);
		List<Vector2> newArray = new List<Vector2>();
		
		for (var i = 0; i < pointArray.Count; i++) {
			float newX = pointArray[i].x + origin.x - center.x;
			float newY = pointArray[i].y + origin.y - center.y;
			newArray.Add(new Vector2(newX, newY));
		}

		return newArray;
	}
	
	// --------------------------------  		     GESTURE OPTIMIZING DONE   		----------------------------------------------------------------
	// -------------------------------- 		START OF THE MATCHING PROCESS	----------------------------------------------------------------

	private void GestureMatchForCurrentTemplate(List<Vector2> pointArray) {
		float tempDistance = Mathf.Infinity;
		var count = 0;
		
		for (int i = 0; i < gestureTemplates.Templates.Count; i++) {
			float distance = CalcDistanceAtOptimalAngle(pointArray, gestureTemplates.Templates[i], -angleRange, angleRange);
			
			if (distance < tempDistance) {
				tempDistance = distance;
				count = i;
			}
		}
		
		float HalfDiagonal = 0.5f * Mathf.Sqrt(Mathf.Pow(sizeOfScaleRect, 2) + Mathf.Pow(sizeOfScaleRect, 2));
		float score = 1.0f - (tempDistance / HalfDiagonal);
		
		// print the result
		
		if (score < 0.7) {
			outputText = "Фигура: Нет совпадения " +  "\n";
		} else {
			if (gestureTemplates.TemplateNames[count].Equals(gestureTemplates.TemplateNames[GameController.currentLevel])) {
				outputText = "Верно!\n" + "Фигура: " + gestureTemplates.TemplateNames[count] + "\n" + "Точность: " + Mathf.Round(100 * score) + "%";
				gameController.SendMessage("NextStage", outputText);
			}
		}
	}

	// --------------------------------  		   GESTURE RECOGNIZER DONE   		----------------------------------------------------------------
	// -------------------------------- 		START OF THE HELP FUNCTIONS		----------------------------------------------------------------
	
	private Vector2 CalcCenterOfGesture(List<Vector2> pointArray) {
		// finds the center of the drawn gesture
		
		float averageX = 0.0f;
		float averageY = 0.0f;
		
		for (int i = 0; i < pointArray.Count; i++) {
			averageX += pointArray[i].x;
			averageY += pointArray[i].y;
		}
		
		averageX = averageX / pointArray.Count;
		averageY = averageY / pointArray.Count;
		
		return new Vector2(averageX, averageY);
	}	
	
	private float CalcDistance(Vector2 point1, Vector2 point2) {
		// distance between two vector points.
		float dx = point2.x - point1.x;
		float dy = point2.y - point1.y;
		
		return Mathf.Sqrt(dx * dx + dy * dy);
	}
	
	private float CalcTotalGestureLength(List<Vector2> pointArray) { 
		// total length of gesture path
		float length = 0.0f;
		for (int i = 1; i < pointArray.Count; i++) {
			length += CalcDistance(pointArray[i - 1], pointArray[i]);
		}

		return length;
	}
	
	private float CalcDistanceAtOptimalAngle(List<Vector2> pointArray, List<Vector2> T, float negativeAngle, float positiveAngle) {
		// Create two temporary distances. Compare while running through the angles. 
		// Each time a lower distace between points and template points are foound store it in one of the temporary variables. 
		
		float radian1 = Mathf.PI * negativeAngle + (1.0f - Mathf.PI ) * positiveAngle;
		float tempDistance1 = CalcDistanceAtAngle(pointArray, T, radian1);
		
		float radian2 = (1.0f - Mathf.PI ) * negativeAngle + Mathf.PI  * positiveAngle;
		float tempDistance2 = CalcDistanceAtAngle(pointArray, T, radian2);
		
		// the higher the number compareDetail is, the better recognition this system will perform. 
		for (int i = 0; i < compareDetail; i++) {
			if (tempDistance1 < tempDistance2)	{
				positiveAngle = radian2;
				radian2 = radian1;
				tempDistance2 = tempDistance1;
				radian1 = Mathf.PI * negativeAngle + (1.0f - Mathf.PI) * positiveAngle;
				tempDistance1 = CalcDistanceAtAngle(pointArray, T, radian1);
			} else {
				negativeAngle = radian1;
				radian1 = radian2;
				tempDistance1 = tempDistance2;
				radian2 = (1.0f - Mathf.PI) * negativeAngle + Mathf.PI * positiveAngle;
				tempDistance2 = CalcDistanceAtAngle(pointArray, T, radian2);
			}
		}
		
		return Mathf.Min(tempDistance1, tempDistance2);
	}
	
	private float CalcDistanceAtAngle(List<Vector2> pointArray, List<Vector2> T, float radians) {
		// calc the distance of template and user gesture at 
		Vector2 center = CalcCenterOfGesture(pointArray);
		List<Vector2> newpoints = RotateGesture(pointArray, radians, center);
		return CalcGestureTemplateDistance(newpoints, T);
	}	
	
	private float CalcGestureTemplateDistance(List<Vector2> newRotatedPoints, List<Vector2> templatePoints) {
		// calc the distance between gesture path from user and the template gesture
		float distance = 0.0f;

		for (int i = 0; i < newRotatedPoints.Count; i++) { // assumes newRotatedPoints.length == templatePoints.length
			/*
			if (newRotatedPoints.Count > templatePoints.Count) {
				throw new Exception("Ebaniy exception! newRotatedPoints.Count=" + newRotatedPoints.Count + ", templatePoints.Count=" + templatePoints.Count);
			}
			*/
			distance += CalcDistance(newRotatedPoints[i], templatePoints[i]);
		}

		return distance / newRotatedPoints.Count;
	}
}
